clc; 
clear;
clf;
sampling_time = 2;
delay = 4;
order = 1;
num = [2 1];
den = conv([10,1],conv([7 1],[3 1]));

plant_c_without_delay = tf(num, den);
% [num_delay, den_delay] = pade(delay, order)
% Ова се веќе пресметани коефициенти за pade апроксимацијата
num_delay = [-1, 0.5];
den_delay = [1, 0.5];
delay = tf(num_delay, den_delay);                

plant_c = plant_c_without_delay * delay;
plant_d = c2d(plant_c, sampling_time);
pole(plant_d)

Gw_d = tf([1], [1 0], sampling_time);
controller_d = 1 / plant_d * Gw_d / (1 - Gw_d);
system_d = feedback(controller_d * plant_d, 1);
step(plant_d, system_d, 200)
