clc; 
clear;
clf;
sampling_time = 2;
delay = 4;
order = 1;
num = [1, 1];
den = [1, 100, 20, 1];

plant_c_without_delay = tf(num, den);

% [num_delay, den_delay] = pade(delay, order)
% Ова се веќе пресметани коефициенти за pade апроксимацијата
num_delay = [-1, 0.5];
den_delay = [1, 0.5];
delay = tf(num_delay, den_delay);

plant_c = plant_c_without_delay * delay;
% step(plant_c_without_delay, plant_c, 100)
[response, time] = step(plant_c);
diff_response = diff(response);
% plot(diff_response);
[value, arg] = max(diff_response);

% Равенка на права низ две точки y = kx + n
xarg1 = arg - 1;
xarg2 = arg + 1;
x1 = time(xarg1);
x2 = time(xarg2);
y1 = response(xarg1);
y2 = response(xarg2);
k = (y2 - y1) / (x2 - x1);
n = y1 - k*x1;
y = k*time + n;

Kp = response(end) / 1 % 1 за единичен отскочен влез
tangent_intersects_Kp = (Kp - n) / k;
tangent_intersects_zero = - n / k;
Tu = tangent_intersects_zero
Tg = tangent_intersects_Kp - tangent_intersects_zero
display(['Tu/Tg = ', num2str(Tu/Tg)])

hold on
plot(time, response)
plot(time, y)
plot(time, Kp*ones(size(time, 1), 1))
plot(time, zeros(size(time, 1), 1))
hold off

% Вредностите за q0, q1 и q2 ги пресметуваме рачно според правилата на Такахаши.

q0 = 1.725;
q1 = -1.84;
q2 = 0.575;
T0 = 6.68;

% На крајот ќе го симулариме нашиот систем со проектираниот управувач. Такахаши 
% се смета за добра појдовна точка при нагодување. Понатаму рачно се коригира 
% одѕивот во зависност од потребите на процесот.

plant_d = c2d(plant_c, T0);
controller_d = tf([q0, q1, q2], [1, -1, 0], T0);
system = feedback(controller_d * plant_d, 1);
% step(system)