import numpy as np
from scipy import linalg
from matplotlib import pyplot as plt
import control


def d2c(system):
    sys_type = 's'
    sys = system
    if isinstance(system, control.TransferFunction):
        sys_type = 'tf'
        sys = control.tf2ss(system)
    n = sys.A.shape[0]
    nb = sys.B.shape[1]
    a, b, c, d = None, None, None, None
    if n == 1:
        if sys.B[0, 0] == 1:
            a = 0
            b = sys.B / sys.dt
            c = sys.C
            d = sys.D
    else:
        tmp1 = np.hstack((sys.A, sys.B))
        tmp2 = np.hstack((np.zeros((nb, n)), np.eye(nb)))
        tmp = np.vstack((tmp1, tmp2))
        s = linalg.logm(tmp)
        s = s / sys.dt
        s = np.real(s)
        a = s[0:n, 0:n]
        b = s[0:n, n:n + nb]
        c = sys.C
        d = sys.D
    sysc = control.StateSpace(a, b, c, d)
    if sys_type == 'tf':
        sysc = control.ss2tf(sysc)
    return sysc


def step(*systems):
    time = np.array([])
    for index, system in enumerate(systems):
        assert isinstance(system, control.TransferFunction)
        if system.dt is None:
            time, step_response = control.step_response(system)
            plt.plot(time, step_response, label=str(index)+' lti')
        else:
            time, step_response = control.step_response(system)
            time = np.linspace(time[0], time[-1], step_response.shape[0])
            plt.step(time, step_response, label=str(index)+' dlti')
        stepinfo(system, index)
    plt.plot(time, np.ones_like(time), label='step')
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Response')
    plt.title('Step response')
    plt.show()


def stepinfo(system, index, verbose=False):
    time, step_response = control.step_response(system)
    square_error = np.sum(step_response ** 2)
    if verbose:
        print('lti {} from {} to {} has integral square error of {}'.format(index, time[0], time[-1], square_error))


def performance_criteria(e, u, r, verbose=False):
    for time, response in [e, u]:
        square_error = np.sum(response ** 2)
        if verbose:
            print('the integral square error from {:.2f} to {:.2f} is {:.6f}'.format(time[0], time[-1], square_error))
    performance = np.sum(e[1] ** 2 + r * u[1] ** 2)
    if verbose:
        print('perfonmance criteria integral e ** 2 + r * u ** 2 is', performance)
    return performance


def discrete_controller(v, coefs, dt, verbose=False):
    assert len(coefs) == v + 1
    if v == 0:
        controller = control.tf(coefs + [0], [1, -1], dt)
    else:
        controller = control.tf(coefs, [1, -1] + [0] * (v - 1), dt)
    if verbose:
        print(controller)
    return controller


def zero_order_discrete_controller(coeff, dt, plant_c, r, plot=False):
    controller_d = discrete_controller(0, coeff, dt)
    try:
        controller_c = d2c(controller_d)
    except IndexError:
        print('замолчи')
        return -1
    g_0_c = controller_c * plant_c
    g_1_c = control.feedback(g_0_c, 1)
    if not is_stable(g_1_c):
        return -1
    g_2_c = control.feedback(controller_c, plant_c)
    t_e, e_response = control.step_response(1 - g_1_c)
    t_u, u_response = control.step_response(g_2_c)
    performance = performance_criteria((t_e, e_response), (t_u, u_response), r)
    if plot:
        step(g_1_c, g_2_c)
    return performance


def first_order_discrete_controller(coeffs, dt, plant_c, r, plot=False):
    controller_d = discrete_controller(1, coeffs, dt)
    try:
        controller_c = d2c(controller_d)
    except IndexError:
        print('замолчи')
        return -1
    g_0_c = controller_c * plant_c
    g_1_c = control.feedback(g_0_c, 1)
    if not is_stable(g_1_c):
        return -1
    g_2_c = control.feedback(controller_c, plant_c)
    t_e, e_response = control.step_response(1 - g_1_c)
    t_u, u_response = control.step_response(g_2_c)
    performance = performance_criteria((t_e, e_response), (t_u, u_response), r)
    if plot:
        step(g_1_c, g_2_c)
    return performance


def tune_zero_order_discrete_controller(dt, plant_c, r):
    tune_coeffs = np.linspace(0, 20, 100)
    performance = [zero_order_discrete_controller([coeff], dt, plant_c, r) for coeff in tune_coeffs]
    plt.plot(tune_coeffs, performance)
    plt.xlabel('Coefficient q_0')
    plt.ylabel('Error')
    plt.title('Zero order discrete controller tuning')
    plt.show()


def tune_first_order_discrete_controller(dt, plant_c, r):
    center = 0, 0
    width = 5
    samples = 20
    ar = np.linspace(center[0] - width, center[0] + width, samples)
    br = np.linspace(center[1] - width, center[1] + width, samples)
    tune_coeffs = [[a, b] for a in ar for b in br]
    performance = [first_order_discrete_controller(coeffs, dt, plant_c, r) for coeffs in tune_coeffs]
    performance = np.array(performance).reshape((ar.shape[0], br.shape[0]))
    maximum = np.max(performance)
    performance[performance == -1] = maximum
    plt.imshow(performance)
    plt.colorbar()
    plt.xlabel('Coefficient q_0')
    plt.ylabel('Coefficient q_1')
    plt.title('First order discrete controller tuning')
    plt.show()


def pid_example(coeffs, plant_c, dt):
    controller_d = discrete_controller(2, coeffs, dt, verbose=True)
    controller_c = d2c(controller_d)
    g_0_c = controller_c * plant_c
    g_1_c = control.feedback(g_0_c, 1)
    g_2_c = control.feedback(controller_c, plant_c)
    step(g_1_c, g_2_c)


def is_stable(system):
    return all(np.real(system.pole()) < 0)


def main():
    dt = 1
    r = 1
    plant_c = control.tf([1], [1, -1])
    print(is_stable(control.feedback(plant_c, 1)))
    plant_c_2 = control.tf([-4, 1], [40, 14, 1])
    print(is_stable(control.feedback(plant_c_2, 1)))
    tune_zero_order_discrete_controller(dt, plant_c, r)
    zero_order_discrete_controller([1.25], 1, plant_c, r, plot=True)
    tune_first_order_discrete_controller(dt, plant_c, r)
    first_order_discrete_controller([1.3, -1.1], 1, plant_c, r, plot=True)
    coeffs = [5.9603, -10.3425, 4.4933]
    pid_example(coeffs, plant_c, dt)


main()
