A = [0, 1;, 2, -1]
B = [1; 0]
C = [1, 0]
D = 0
sys = ss(A, B, C, D)
E = eig(A)

P = [-2, -1]
K = place(A, B, P)

Acl = A - B*K
Ecl = eig(A - B*K)

syscl = ss(Acl, B, C, D)
step(syscl)

Kdc = dcgain(syscl)
Kr = 1 / Kdc

syscl_scaled = ss(Acl, B*Kr, C, D)
step(syscl_scaled)
