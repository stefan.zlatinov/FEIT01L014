clc
clear

% Нека е даден некој дискретен систем
A = [0, 1, 0; 0, 0, 1; -0.0125, -0.2, -0.85];
B = [0; 0; 1];
C = [1, 0, 0];
D = 0;
% со некое време на одбирање
sampling_time = 10;

% ова е моделот во просторот на состојби со кој располагаме
system = ss(A, B, C, D, sampling_time);

% ова се случајно избрани полови, вредности кои сакаме да ги поставиме како 
% полови на системот откако ќе вметнеме негативна повратна врска по состојбите 
P = [0.3, 0.3, 0.3]
% функцијата place() ќе ја одреди вреднноста на коефициентите во векторот K
K = place(system, P)

% останува го видиме излезот
system_closed = ss(A - B*K, B, C, D, sampling_time)
% и да ја испитаме неговата стабилност
pole(system_closed)

% за да ја поправиме стационарната грешка потребно е матрицата B да ја скалираме
Kdc = dcgain(system_closed)
system_closed_scaled = ss(A - B*K, B / Kdc, C, D, sampling_time)

% еве го посакуваното поведение 
step(system_closed, system_closed_scaled, 150)

